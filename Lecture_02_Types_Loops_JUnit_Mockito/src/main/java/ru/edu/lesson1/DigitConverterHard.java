package ru.edu.lesson1;

/**
 * Задача со звездочкой, конвертор с дополнительным методом для работы с вещественными числами
 */
public interface DigitConverterHard extends DigitConverter {
    /**
     * Преобразование вещественного десятичного числа в другую систему счисления
     *
     * @param digit     - число
     * @param radix     - основание системы счисления
     * @param precision - точность, сколько знаков после '.'
     * @throws IllegalArgumentException если digit отрицательное число
     * @throws IllegalArgumentException если radix меньше единицы
     */
    String convert(double digit, int radix, int precision);
}
